function add_stress_colors_to_plots_with_offsets(ranges, start_range, end_range, max_height, min_height) 
    relax1_row_number = 1;
    physical_stress_row_number = 2;
    relax2_row_number = 3;
    emotional_stress1_row_number = 4;
    cognitive_stress_row_number = 5;
    relax3_row_number = 6;
    emotional_stress2_row_number = 7;
    relax4_row_number = 8;
    
    fills_added = [];
    range_names_added = string([]);
    
    hold on;
    firstrange = ranges(start_range, :);
    offset_index = firstrange(1,1);
    offset_index = [offset_index, offset_index];
    offset_index;
    for range_i=start_range:end_range
        range = ranges(range_i, :);
        color = "k";
        range_name = "None";
        if (range_i == relax1_row_number)
            color = "b";
            range_name = "Relax";
        end
        if (range_i == physical_stress_row_number)
            color = "r";
            range_name = "Physical Stress";
        end
        if (range_i == relax2_row_number)
            color = "b";
            range_name = "Relax";
        end
        if (range_i == emotional_stress1_row_number)
            color = "g";
            range_name = "Emotional Stress";
        end
        if (range_i == cognitive_stress_row_number)
            color = "y";
            range_name = "Cognitive Stress";
        end
        if (range_i == relax3_row_number)
            color = "b";
            range_name = "Relax";
        end
        if (range_i == emotional_stress2_row_number)
            color = "g";
            range_name = "Emotional Stress";
        end
        if (range_i == relax4_row_number)
            color = "b";
            range_name = "Relax";
        end
        height_mult_high = 1;
        height_mult_low = 1;
        h = fill([range(1,:) - offset_index, flip(range(1,:)) - offset_index],[min_height*height_mult_low, min_height*height_mult_low, max_height*height_mult_high, max_height*height_mult_high], color, 'LineStyle', 'none'); 
        set(h,'facealpha',.2)
        fills_added = [fills_added,h];
        range_names_added = [range_names_added,range_name];
    end
    col_count = size(range_names_added);
    lgd = legend(fills_added, range_names_added, 'Location', 'east');
    lgd.NumColumns = col_count(1);
    hold off;
end
