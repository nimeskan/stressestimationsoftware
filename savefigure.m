function savefigure(filename)
	% save figure as image and fig file
    set(gcf, 'PaperUnits', 'inches');
    set(gcf, 'PaperPosition', [0 0 15 10]);
    set(findall(gcf,'-property','FontSize'),'FontSize',13)
    saveas(gcf, filename + '.png');
    saveas(gcf, filename + '.fig');
end