% Nima Eskandari
% Nima.Eskandari@outlook.com
% University of Houston March 2020

clear all;
close all;
clc;
% This is a switch to decide whether visualization graphs are created or not
graph_signals = true;

% Set font size
set(0,'defaulttextfontsize',10);
set(0,'defaultaxesfontsize',10);

% The following are used along with get_data function to select which data to get
subject_name_column_number = 1;
acc_temp_eda_column_number = 2;
spo2_hr_column_number = 3;
temp_column_number = 7;
eda_column_number = 8;
ate_label_column_number = 9;
heartrate_column_number = 4;
spo2_column_number = 5;
sh_label_column_number = 6;


% The following are used along with add_stress_colors_to_plots
% function to select which data to get
relax1_row_number = 1;
physical_stress_row_number = 2;
relax2_row_number = 3;
emotional_stress1_row_number = 4;
cognitive_stress_row_number = 5;
relax3_row_number = 6;
emotional_stress2_row_number = 7;
relax4_row_number = 8;

% File names for the dataset
acc_temp_eda_file_name = "AccTempEDA.csv";
spo2_hr_file_name = "SpO2HR.csv";
dataset_folder = "../HealthySubjectsBiosignalsDataSet";

% Selected subjects
selected_subjects = [1,5,8,9,12,16];

% load deconvolution data
load("Arousal_Tracking_Results.mat");

subject_names = string([]);

% Create the subject names
for sub_i=1:20
    subject_names(sub_i) = "Subject" + int2str(sub_i);
end

% Read the dataset and populate the subject_dfs table
subject_dfs = table();
for sub_i=selected_subjects
    subject_name = subject_names(sub_i);
    subject_folder = dataset_folder + "/" + subject_name;
    subject_acc_temp_eda_file = subject_folder + "/" + subject_name + acc_temp_eda_file_name;
    subject_spo2_hr_file = subject_folder + "/" + subject_name + spo2_hr_file_name;
    subject_acc_temp_eda_df = readtable(subject_acc_temp_eda_file);
    subject_spo2_hr_df = readtable(subject_spo2_hr_file);
    acc_temp_eda_column = {(subject_acc_temp_eda_df)};
    spo2_hr_column = {(subject_spo2_hr_df)};
    subject_dfs = [subject_dfs; table(subject_name, acc_temp_eda_column, spo2_hr_column)];
end

% This is the findpeaks threshold number
PEAK_THRESHOLD = 0.10;

% Runnnig on multiple CPUs
%pc = parcluster('local');
%parpool(pc, str2num(getenv('SLURM_CPUS_ON_NODE')));

% run a parfor loop, distributing the iterations to the SLURM_CPUS_ON_NODE
for sub_i = 1:6
    disp("Subject index: " + int2str(sub_i));
    
	% Read the EDA, TEMP, SPO2 and HR
    eda = get_data(subject_dfs, sub_i, acc_temp_eda_column_number, eda_column_number);
    temp = get_data(subject_dfs, sub_i, acc_temp_eda_column_number, temp_column_number);
    spo2 = get_data(subject_dfs, sub_i, spo2_hr_column_number, spo2_column_number);
    hr = get_data(subject_dfs, sub_i, spo2_hr_column_number, heartrate_column_number);
    labels = get_data(subject_dfs, sub_i, acc_temp_eda_column_number, ate_label_column_number);
    
    % Find the physical stress range on the x axis
    physical_stress_indexes = find(strcmp(labels, 'PhysicalStress'));
    physical_stress_range = [physical_stress_indexes(1), physical_stress_indexes(end)];
    
    % Find the relax1 range on the x axis
    relax1_indexes = find(strcmp(labels(1:physical_stress_range(1)), 'Relax'));
    relax1_range = [1, relax1_indexes(end)];
    
    % Find the cognitive stress range on the x axis
    cognitive_stress_indexes = find(strcmp(labels, 'CognitiveStress'));
    cognitive_stress_range = [cognitive_stress_indexes(1), cognitive_stress_indexes(end)];
    
    % Find the relax2 range on the x axis
    relax2_indexes = find(strcmp(labels(physical_stress_range(2):cognitive_stress_range(1)), 'Relax'));
    relax2_range = [physical_stress_range(2) + relax2_indexes(1) - 1, physical_stress_range(2) + relax2_indexes(end) - 1];
    
    % Find the emotional stress 1 range on the x axis
    emotional_stress1_indexes = find(strcmp(labels(relax2_range(2):cognitive_stress_range(1)), 'EmotionalStress'));
    emotional_stress1_range = [relax2_range(2) + emotional_stress1_indexes(1) - 1, relax2_range(2) + emotional_stress1_indexes(end) - 1];
    
    % Find the emotional stress 2 range on the x axis
    emotional_stress2_indexes = find(strcmp(labels(cognitive_stress_range(1):end), 'EmotionalStress'));
    emotional_stress2_range = [cognitive_stress_range(1) + emotional_stress2_indexes(1) - 1, cognitive_stress_range(1) + emotional_stress2_indexes(end) - 1];
    
    % Find the relax3 range on the x axis
    relax3_indexes = find(strcmp(labels(cognitive_stress_range(1):emotional_stress2_range(1)), 'Relax'));
    relax3_range = [cognitive_stress_range(1) + relax3_indexes(1) - 1, cognitive_stress_range(1) + relax3_indexes(end) - 1];
    
    % Find the relax4 range on the x axis
    relax4_indexes = find(strcmp(labels(emotional_stress2_range(2):end), 'Relax'));
    relax4_range = [emotional_stress2_range(2) + relax4_indexes(1) - 1, emotional_stress2_range(2) + relax4_indexes(end) - 1];
    
    % Put all ranges in an array
    ranges = [relax1_range; physical_stress_range; relax2_range; emotional_stress1_range; cognitive_stress_range; relax3_range; emotional_stress2_range; relax4_range];
    
    % Upsample the heart rate and SpO2 signals
    eda_size = size(eda);
    spo2_size = size(spo2);
    
    upsample_rate = round(eda_size(1)/spo2_size(1));
    
    spo2_upsampled = kron(spo2, ones(upsample_rate, 1));
    hr_upsampled = kron(hr, ones(upsample_rate, 1));
    
    spo2_upsampled = spo2_upsampled(1:eda_size(1), :);
    hr_upsampled = hr_upsampled(1:eda_size(1), :);
    
    relax4_range(2) = eda_size(1);
    
    eda_binary_dec = zeros(eda_size(1),1);
    valid_binary_dec = kron((subject(sub_i).binary)', ones(upsample_rate, 1));
    size_valid_binary_dec = size(valid_binary_dec);
    eda_binary_dec(emotional_stress1_range(1):emotional_stress1_range(1) + size_valid_binary_dec(1) - 1) = valid_binary_dec;
    

    % Normalize the data
    normalized_eda = zscore(eda);
    % Get the phasic and tonic part of the EDA
    [phasic, sparse, tonic, l,d,e,o] = cvxEDA(normalized_eda, 1/8);
    [pks, locs] = findpeaks(phasic);
    number_of_peaks = size(pks);
    one_pks = zeros(eda_size(1), 1);
    one_pks(locs(pks > PEAK_THRESHOLD)) = 1;
    temp_zscore = zscore(temp);
    spo2_zscore = zscore(spo2_upsampled);
    hr_zscore = zscore(hr_upsampled);
    % Smooth the data
    temp_smooth = smoothdata(temp_zscore);
    spo2_smooth = smoothdata(spo2_zscore);
    hr_smooth = smoothdata(hr_zscore);

    % Invert the heart rate and shift up so there are no neg values
    hr_inverted = -1*hr_smooth;
    if (min(hr_inverted) < 0)
        hr_inverted = hr_inverted - min(hr_inverted) + 1.01;
    end
    
    % Shift up the Temp so there are no neg values
    temp_inverted = 1*temp_smooth;
    if (min(temp_inverted) < 0)
        temp_inverted = temp_inverted - min(temp_inverted) + 1.01;
    end
    
    % Invert the SpO2 and shift up so there are no neg values
    spo2_inverted = -1*spo2_smooth;
    if (min(spo2_inverted) < 0)
        spo2_inverted = spo2_inverted - min(spo2_inverted) + 1.01;
    end

    % Graph the visualization of the data
    if graph_signals
	    figure()
	    subplot(4,1,1)
	    plot(normalized_eda, 'color', 'k');
	    title("Normalized EDA");
	    add_stress_colors_to_plots(ranges, relax1_row_number, relax4_row_number, max(normalized_eda), min(normalized_eda));
	    
	    subplot(4,1,2);
	    plot(phasic, 'color', 'r');
	    title("Phasic EDA");    

	    subplot(4,1,3);
	    plot(eda_binary_dec, 'color', 'k');
	    title("Tonic EDA");

	    subplot(4,1,4);
	    plot(one_pks, 'color', 'r');
	    title("Phasic Peaks EDA");
	    xlabel("Time Index (125 ms)");
	    %savefigure("images/Subject" + int2str(sub_i) + "/Subject" + int2str(sub_i) + "eda");

	    figure();
	    subplot(2,2,1);
	    plot(temp, 'color', 'k');
	    title("Body Temperature");

	    subplot(2,2,2);
	    plot(temp_zscore, 'color', 'k');
	    title("Body Temperature Standardized");
	    
	    subplot(2,2,3);
	    plot(hr_upsampled, 'color', 'k');
	    title("Heart Rate");
	    
	    subplot(2,2,4);
	    plot(hr_zscore, 'color', 'k');
	    title("Heart Rate Standardized");
	    %savefigure("images/Subject" + int2str(sub_i) + "/Subject" + int2str(sub_i) + "standardization");
	    
	    figure();
	    subplot(2,1,1);
	    plot(temp_smooth, 'color', 'k');
	    title("Temperature Smooth");    
	    add_stress_colors_to_plots(ranges, relax1_row_number, relax4_row_number, max(temp_smooth), min(temp_smooth));
	    
	    subplot(2,1,2);
	    plot(hr_smooth, 'color', 'k');
	    title("Heart Rate Smooth");
	    xlabel("Time Index (125 ms)");
	    %savefigure("images/Subject" + int2str(sub_i) + "/Subject" + int2str(sub_i) + "smoothing");
	end
%     
%     % Create a binary array of whether stress exists (1) or not (0)
%     stress_binary_ground_truth = zeros(eda_size(1), 1);
%     stress_binary_ground_truth(physical_stress_range(1):physical_stress_range(2)) = 1;
%     stress_binary_ground_truth(emotional_stress1_range(1):emotional_stress1_range(2)) = 1;
%     stress_binary_ground_truth(cognitive_stress_range(1):cognitive_stress_range(2)) = 1;
%     stress_binary_ground_truth(emotional_stress2_range(1):emotional_stress2_range(2)) = 1;
%     
%     % Pick the range to work with for our algorithms
%     start_index = cognitive_stress_range(1);
%     end_index = relax3_range(2);
%     
%     % Run and graph the one binary two continuous
%     disp("Subject " + int2str(sub_i) + " Binary SCR with temperature and heart rate");
%     x_smth = runBinTwoContinuous(stress_binary_ground_truth(start_index:end_index,:)', one_pks(start_index:end_index,:)', temp_smooth(start_index:end_index,:)', hr_smooth(start_index:end_index,:)', "Temperature", "Heart Rate");
%     add_stress_colors_to_plots_with_offsets(ranges, cognitive_stress_row_number, relax3_row_number, max(x_smth), min(x_smth));
%     savefigure("images/Subject" + int2str(sub_i) + "/Subject" + int2str(sub_i) + "hrtemp");
%     % disp("Subject " + int2str(sub_i) + " Binary SCR with SpO2 and heart rate");
%     % x_smth = runBinTwoContinuous(stress_binary_ground_truth(start_index:end_index,:)', one_pks(start_index:end_index,:)', spo2_smooth(start_index:end_index,:)', hr_smooth(start_index:end_index,:)', "SpO2", "Heart Rate");
%     % add_stress_colors_to_plots_with_offsets(ranges, cognitive_stress_row_number, relax3_row_number, max(x_smth), min(x_smth));
%     % savefigure("images/Subject" + int2str(sub_i) + "/Subject" + int2str(sub_i) + "spo2hr");
%     % disp("Subject " + int2str(sub_i) + " Binary SCR with SpO2 and temperature");
%     % x_smth = runBinTwoContinuous(stress_binary_ground_truth(start_index:end_index,:)', one_pks(start_index:end_index,:)', spo2_smooth(start_index:end_index,:)', temp_smooth(start_index:end_index,:)', "SpO2", "Temperature");
%     % add_stress_colors_to_plots_with_offsets(ranges, cognitive_stress_row_number, relax3_row_number, max(x_smth), min(x_smth));
%     % savefigure("images/Subject" + int2str(sub_i) + "/Subject" + int2str(sub_i) + "spo2temp");
%     
%     % Visualization graph for the continuous signals
%     if graph_signals
% 	    figure()
% 	    plot(hr_inverted, 'color', 'k');
% 	    title("Preprocessed Heart Rate for One Binary and One Continuous Valued Data Method");
% 	    add_stress_colors_to_plots(ranges, relax1_row_number, relax4_row_number, max(hr_inverted), min(hr_inverted))
% 	    savefigure("images/Subject" + int2str(sub_i) + "/Subject" + int2str(sub_i) + "hrpreproc");
% 
% 	    figure()
% 	    plot(temp_inverted, 'color', 'k');
% 	    title("Preprocessed Temperature for One Binary and One Continuous Valued Data Method");
% 	    add_stress_colors_to_plots(ranges, relax1_row_number, relax4_row_number, max(temp_inverted), min(temp_inverted))
% 	    savefigure("images/Subject" + int2str(sub_i) + "/Subject" + int2str(sub_i) + "temppreproc");
% 
% 	    figure()
% 	    plot(spo2_inverted, 'color', 'k');
% 	    title("Preprocessed SpO2 Level for One Binary and One Continuous Valued Data Method");
% 	    add_stress_colors_to_plots(ranges, relax1_row_number, relax4_row_number, max(spo2_inverted), min(spo2_inverted))
% 	    savefigure("images/Subject" + int2str(sub_i) + "/Subject" + int2str(sub_i) + "spo2preproc");
% 	end
% 
%     % Run and draw one binary and one continuous
%     disp("Subject " + int2str(sub_i) + " Binary SCR with heart rate");
%     try
%         x_smth = runEMexample(one_pks(start_index:end_index,:)', hr_inverted(start_index:end_index,:)', sub_i, "Heart Rate");
%         add_stress_colors_to_plots_with_offsets(ranges, cognitive_stress_row_number, relax3_row_number, max(x_smth), min(x_smth));
%         savefigure("images/Subject" + int2str(sub_i) + "/Subject" + int2str(sub_i) + "scrhr");
%     catch
%         disp("FAILED: Subject " + int2str(sub_i) + " Binary SCR with heart rate");
%     end
%     disp("Subject " + int2str(sub_i) + " Binary SCR with temperature");
%     try
%         x_smth = runEMexample(one_pks(start_index:end_index,:)', temp_inverted(start_index:end_index,:)', sub_i, "Temperature");
%         add_stress_colors_to_plots_with_offsets(ranges, cognitive_stress_row_number, relax3_row_number, max(x_smth), min(x_smth));
%         savefigure("images/Subject" + int2str(sub_i) + "/Subject" + int2str(sub_i) + "scrtemp");
%     catch
%         disp("FAILED: Subject " + int2str(sub_i) + " Binary SCR with temperature");
%     end
%     % disp("Subject " + int2str(sub_i) + " Binary SCR with spo2");
%     % try
%     %     x_smth = runEMexample(one_pks(start_index:end_index,:)', spo2_inverted(start_index:end_index,:)', sub_i, "SpO2");
%     %     add_stress_colors_to_plots_with_offsets(ranges, cognitive_stress_row_number, relax3_row_number, max(x_smth), min(x_smth));
%     %     savefigure("images/Subject" + int2str(sub_i) + "/Subject" + int2str(sub_i) + "scrspo2");
%     % catch
%     %     disp("FAILED: Subject " + int2str(sub_i) + " Binary SCR with spo2");
%     % end
end 
