function xnew = runEMexample(n, z, sub_i, z_label)
% Algorithm for one binary one continuous
eval('pathnow= what');
addpath([pathnow.path '/mixed']);

N = n;  
Z = z;  % Z is log(reaction time)

pcrit = 0.95;
backprobg = 0.2; %starting probability of correct
startflag = 0;  %this can take values 0 or 2**
                %0 fixes the initial condition at backprobg
                %2 initial condition is estimated

%these are all starting guesses
ialpha = Z(1); %guess it's the first value of Z
ibeta  = -1;
isig2e = 0.05;
isig2v = 0.05;
irho = 1; %fixed in this code


%RUN EM ESTIMATION

[alph, beta, gamma, rho, sig2e, sig2v, xnew, signewsq, muone, a] ...
                                 = mixedlearningcurve2(N, Z, backprobg, irho, ialpha, ibeta, isig2e, isig2v, startflag, sub_i);
                                                   
%[r05, r95, rmid, rmode, rmatrix] = rdistn(xnew, signewsq, .5, alph, beta);
%[b05, b95, bmid, bmode, pmatrix] = binpdistn(xnew, signewsq, muone, .5);



%Calculate Mixed learning trial

samplesfirsttrial = normrnd(xnew(1), sqrt(signewsq(1)),1,10000);
%go backwards thro' data to find last trial where greater than first trial
for t = length(N):-1:2
    allsamples  = normrnd(xnew(t), sqrt(signewsq(t)),1,10000);
    prop_bigger = length(find((samplesfirsttrial-allsamples) <0 ))/length(allsamples);
    if(prop_bigger < pcrit)
        learning_trial = t+1; break
    end
end

t=1:length(N);
%Plotting stuff
figure
subplot(311)
plot(t, n,'color', 'r')
ylabel('SCR Binary Driver')
title("Participant " + num2str(sub_i) + " Stress Estimation");
subplot(312)
hold on
%fill([t fliplr(t)], [r05(2:end) fliplr(r95(2:end))] , [0.9 0.7 0.7])
%plot(t,rmid(2:end),'k')
%plot(t,r05(2:end),'k-')
%plot(t,r95(2:end),'k-')

plot(t, Z,'k','MarkerSize',1);
ylabel(z_label)%'Continuus Signal')
%title('EM')
axis tight; box on;

subplot(313)
ubound = xnew(2:end)+1.645*sqrt(signewsq(2:end));
lbound = xnew(2:end)-1.645*sqrt(signewsq(2:end));
hold on
%fill([t fliplr(t)], [lbound fliplr(ubound)] , [0.9 0.7 0.7])
plot(t,xnew(2:end),'k')
%plot(t,ubound,'k-')
%plot(t,lbound,'k-')

%hold on;
%line([learning_trial learning_trial],[min([min(xnew) min(lbound)]) max([max(ubound) max(xnew)])],'color',[0 1 0]);
%hold on;
%title(['Learning trial is ' num2str(learning_trial) ' with p > ' num2str(pcrit)])
%axis([1 t(end) min(lbound) max(ubound)])
ylabel('Estimated State')

axis tight; box on;

%subplot(414)
%hold on
%fill([t fliplr(t)], [b05(2:end) fliplr(b95(2:end))] , [0.9 0.7 0.7])
%plot(t,bmid(2:end),'k')
%plot(t,b05(2:end),'k-')
%plot(t,b95(2:end),'k-')
%[y, x] = find(N == 0);
%h = plot(x,y+0.05,'s');
%set(h, 'MarkerFaceColor',[.75 .95 .95],'MarkerSize',3);
%set(h, 'MarkerEdgeColor', [0.3 0.3 0.3]);
%[y, x] = find(N > 0);
%h = plot(x,y+0.05,'s'); set(h, 'MarkerFaceColor', 'k');
%set(h, 'MarkerEdgeColor', [0.3 0.3 0.3],'MarkerSize',3);

%title('Confidence interval for the estimated state')
xlabel('Time Index')
axis tight; box on

end



