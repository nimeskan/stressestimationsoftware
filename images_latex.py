
eda_temp = """\\begin{figure}
  \\includegraphics[width=3in]{Subject1eda.png}
  \\caption{Subject 1 EDA}
  \\label{fig:subject1eda}
\\end{figure}"""


std_temp = """\\begin{figure}
  \\includegraphics[width=3in]{Subject1standardization.png}
  \\caption{Subject 1 Temperature, SpO2 and Heart Rate}
  \\label{fig:subject1TempSpO2HeartRate}
\\end{figure}"""

smooth_temp = """\\begin{figure}
  \\includegraphics[width=3in]{Subject1smoothing.png}
  \\caption{Subject 1 Temperature, SpO2 and Heart Rate after Smoothing.}
  \\label{fig:subject1TempSpO2HeartRateSmooth}
\\end{figure}"""

scrhr_temp = """\\begin{figure}
  \\includegraphics[width=3in]{Subject1scrhr.png}
  \\caption{Subject 1's Estimated Stress State From SCR Driver and Heart Rate.}
  \\label{fig:subject1SCRHR}
\\end{figure}"""


scrspo2_temp = """\\begin{figure}
  \\includegraphics[width=3in]{Subject1scrspo2.png}
  \\caption{Subject 1's Estimated Stress State From SCR Driver and SpO2.}
  \\label{fig:subject1SCRHR}
\\end{figure}"""


scrtemp_temp = """\\begin{figure}
  \\includegraphics[width=3in]{Subject1scrtemp.png}
  \\caption{Subject 1's Estimated Stress State From SCR Driver and Temp.}
  \\label{fig:subject1SCRHR}
\\end{figure}"""


hrtemp_temp = """\\begin{figure}
  \\includegraphics[width=3in]{Subject1hrtemp.png}
  \\caption{Subject 1's Stress State Estimated by SCR Binary Feature along with Body Temperature and Heart Rate as Continuous Features.}
  \\label{fig:subject1SCRTempHR}
\\end{figure}"""


spo2hr_temp = """\\begin{figure}
  \\includegraphics[width=3in]{Subject1spo2hr.png}
  \\caption{Subject 1's Stress State Estimated by SCR Binary Feature along with SpO2 Level and Heart Rate as Continuous Features.}
  \\label{fig:subject1SCRSpO2pHR}
\\end{figure}"""


spo2temp_temp = """\\begin{figure}
  \\includegraphics[width=3in]{Subject1spo2temp.png}
  \\caption{Subject 1's Stress State Estimated by SCR Binary Feature along with SpO2 Level and Body Temperature as Continuous Features.}
  \\label{fig:subject1SCRSpO2Temp}
\\end{figure}"""

template = """
\\begin{figure}%
    \\centering
    \\subfloat[Subject 1]{{\\includegraphics[width=3in]{Images/Subject1_NAME_.png} }}%
    \\qquad
    \\subfloat[Subject 2]{{\\includegraphics[width=3in]{Images/Subject2_NAME_.png} }}%
    \\qquad
    \\subfloat[Subject 3]{{\\includegraphics[width=3in]{Images/Subject3_NAME_.png} }}%
    \\qquad
    \\subfloat[Subject 4]{{\\includegraphics[width=3in]{Images/Subject4_NAME_.png} }}%
    \\qquad
    \\subfloat[Subject 5]{{\\includegraphics[width=3in]{Images/Subject5_NAME_.png} }}%
    \\qquad
    \\subfloat[Subject 6]{{\\includegraphics[width=3in]{Images/Subject6_NAME_.png} }}%
    \\caption{EDA}%
    \\label{fig:example}%
\\end{figure}
"""


#for i in range(1,7):
#  print(eda_temp.replace("1", str(i)) + "\n")
#for i in range(1,7):
#  print(std_temp.replace("1", str(i))+ "\n")
#for i in range(1,7):
#  print(smooth_temp.replace("1", str(i))+ "\n")
#for i in range(1,7):
#  print(scrhr_temp.replace("1", str(i))+ "\n")
#for i in range(1,7):
#  print(scrspo2_temp.replace("1", str(i))+ "\n")
#for i in range(1,7):
#  print(scrtemp_temp.replace("1", str(i))+ "\n")
#for i in range(1,7):
#  print(hrtemp_temp.replace("1", str(i))+ "\n")
#for i in range(1,7):
#  print(spo2hr_temp.replace("1", str(i))+ "\n")
#for i in range(1,7):
#  print(spo2temp_temp.replace("1", str(i))+ "\n")
  

print(template.replace("_NAME_", "eda"))
print(template.replace("_NAME_", "standardization"))
print(template.replace("_NAME_", "smoothing"))
print(template.replace("_NAME_", "scrhr"))
print(template.replace("_NAME_", "scrspo2"))
print(template.replace("_NAME_", "scrtemp"))
print(template.replace("_NAME_", "hrtemp"))
print(template.replace("_NAME_", "spo2hr"))
print(template.replace("_NAME_", "spo2temp"))

