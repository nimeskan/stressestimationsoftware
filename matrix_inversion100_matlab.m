%a short demo follows (pseudoinversion of 100 100x100 matrices): 

n = 100; 
m = 100; 
disp ('create 100 random 100x100 matrices'); 
a = rand (n, n, m); 
a = mat2cell (a, n, n, ones (1, m)); 
a = a(:); 
disp ('calculate pseudoinverses - uniprocess'); 
tic; 
p = cellfun (@pinv, a, 'UniformOutput', false); 
toc 
