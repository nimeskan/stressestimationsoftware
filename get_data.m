function data_points = get_data(subject_dfs, subject_number, dataset_column, feature_column)
% get the data from the subject_dfs dataframes
data_points = subject_dfs{subject_number, dataset_column}{1,1}{:, feature_column};
end
